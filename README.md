# RailsBlogFaqManger
## One of the first things I did using Ruby on Rails

This one of my first labs done using Ruby on Rails, in this lab I created a simple blog and a simple FAQ manager with add, edit and delete buttons for each blog/faq to allow easy modification.

### SETUP/USAGE/HOW TO
To see this on your server or site, you need to download this all as a folder then extract it onto the sites folder for ruby (usually C:/Sites) then use ruby command line and start the server when you have navigated into the extracted folder.

### LICENSE
[GLWTPL (Good Luck With That Public License)](https://bitbucket.org/Pronfu/railsblogfaqmanager/src/master/license.txt)

### FAQ / CONTACT / TROUBLESHOOT
* I do not provide support for this as this is my first program using Ruby On Rails and I'm learning to become better using this lanuage.
* There is potential security vulnerabilties in blog/Gemfile.lock and they should be updated (I made this back in 2016 without me or the professor knowing very much about Ruby on Rails).
* Do not contact me regards this, as I will not answer you.
